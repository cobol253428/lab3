       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA5.
       AUTHOR. Natthakritta.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 Grade-data PIC X(90) VALUE "39030261WORAWIT         886345593B
      -    " 886352593D+886342193B+886478593C 886481592C+886491591A ".
       01 Grade.
          03 Stu-ID              PIC   9(8).
          03 Student-Name        PIC   X(16).
          03 Sub1.
             05 Sub-Code1        PIC   9(8).
             05 Sub-Unit1        PIC   9.
             05 Sub-Grade1       PIC   X(2).
          03 Sub2.
             05 Sub-Code2        PIC   9(8).
             05 Sub-Unit2        PIC   9.
             05 Sub-Grade2       PIC   X(2).
          03 Sub3.
             05 Sub-Code3        PIC   9(8).
             05 Sub-Unit3        PIC   9.
             05 Sub-Grade3       PIC   X(2).
          03 Sub4.
             05 Sub-Code4        PIC   9(8).
             05 Sub-Unit4        PIC   9.
             05 Sub-Grade4       PIC   X(2).
          03 Sub5.
             05 Sub-Code5        PIC   9(8).
             05 Sub-Unit5        PIC   9.
             05 Sub-Grade5       PIC   X(2).
          03 Sub6.
             05 Sub-Code6        PIC   9(8).
             05 Sub-Unit6        PIC   9.
             05 Sub-Grade6       PIC   X(2).
                66 Student-ID RENAMES Stu-ID.
                66 Student-Info RENAMES Stu-ID THRU Student-Name.
       01 STUCode REDEFINES Grade.
          05 Student-Year        PIC   9(2).
          05 FILLER              PIC  X(6).
          05 Student-Short-Name  PIC   X(3).
       77 Total-Cost             PIC   9(4)V99.
       
       PROCEDURE DIVISION .
       BEGIN.
           MOVE Grade-data TO Grade.
      *    DISPLAY Grade.

           DISPLAY "Subject 1"
           DISPLAY "Code: " Sub-Code1 
           DISPLAY "Unit: " Sub-Unit1 
           DISPLAY "Grade: " Sub-Grade1 
           DISPLAY "-------------------------"

           DISPLAY "Subject 2"
           DISPLAY "Code: " Sub-Code2 
           DISPLAY "Unit: " Sub-Unit2  
           DISPLAY "Grade: " Sub-Grade2 
           DISPLAY "-------------------------"

           DISPLAY "Subject 3"
           DISPLAY "Code: " Sub-Code3 
           DISPLAY "Unit: " Sub-Unit3 
           DISPLAY "Grade: " Sub-Grade3 
           DISPLAY "-------------------------"

           DISPLAY "Subject 4"
           DISPLAY "Code: " Sub-Code4 
           DISPLAY "Unit: " Sub-Unit4 
           DISPLAY "Grade: " Sub-Grade4 
           DISPLAY "-------------------------"

           DISPLAY "Subject 5"
           DISPLAY "Code: " Sub-Code5 
           DISPLAY "Unit: " Sub-Unit5 
           DISPLAY "Grade: " Sub-Grade5 
           DISPLAY "-------------------------"

           DISPLAY "Subject 6"
           DISPLAY "Code: " Sub-Code6 
           DISPLAY "Unit: " Sub-Unit6 
           DISPLAY "Grade: " Sub-Grade6 
           DISPLAY "-------------------------"
       
           DISPLAY Sub3.
           DISPLAY Stu-ID.
           DISPLAY Student-Info.
           DISPLAY "-------------------------"
             
           DISPLAY Student-Year Student-Short-Name.
           GOBACK.