       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA3.
       AUTHOR. NATTHAKRITTA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SURNAME     PIC   X(8)     VALUE "COUGHLAN".
       01  SALE-PRICE  PIC   9(4)V99.
       01  NUM-OF-EMP  PIC   999V99.
       01  SALARY      PIC   9999V99.
       01  COUNTY-NAME PIC   X(99).

       PROCEDURE DIVISION.
       Begin.
           DISPLAY "1 " SURNAME.
           MOVE "SMITH" TO SURNAME 
           DISPLAY "2 " SURNAME.
           MOVE "FITWILLIAM" TO SURNAME 
           DISPLAY "3 " SURNAME.

           DISPLAY ""
           DISPLAY "1 " SALE-PRICE.
           MOVE ZEROES TO SALE-PRICE
           DISPLAY "2 " SALE-PRICE.
           MOVE 25.5 TO SALE-PRICE
      *    ON SIZE ERROR
           DISPLAY "3 " SALE-PRICE.
           MOVE 7.553 TO SALE-PRICE
           DISPLAY "4 " SALE-PRICE.
           MOVE 93425.158 TO SALE-PRICE
           DISPLAY "5 " SALE-PRICE.

           MOVE 128 TO SALE-PRICE
           DISPLAY "6 " SALE-PRICE.

           DISPLAY ""
      *    01  NUM-OF-EMP  PIC   999V99.
           DISPLAY "1 " NUM-OF-EMP.
      *    guess NUM-OF-EMP 12.4 >> 012.40
           MOVE 12.4 TO NUM-OF-EMP.
           DISPLAY "2 " NUM-OF-EMP.
      *    guess NUM-OF-EMP 6745 >> 745.00
           MOVE 6745 TO NUM-OF-EMP.
           DISPLAY "3 " NUM-OF-EMP.

           DISPLAY ""
      *    01  SALARY      PIC   9999V99.
           DISPLAY "1 " SALARY.
      *    guess NUM-OF-EMP 745.00 >> 0745.00
           MOVE NUM-OF-EMP TO SALARY
           DISPLAY "2 " SALARY.

           DISPLAY ""
           MOVE "GALWAY" TO COUNTY-NAME 
           DISPLAY COUNTY-NAME.
           MOVE ALL "*-" TO COUNTY-NAME 
           DISPLAY COUNTY-NAME.