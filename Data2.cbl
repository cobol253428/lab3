       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA2.
       AUTHOR. NATTHAKRITTA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  ALPHA-NUM   PIC   X(5)        VALUE "A1234".                         
       01  NUM-INT     PIC   9(5).
       01  NUM-NON-INT PIC   9(3)V9(2).
       01  ALPHA       PIC   A(5). 

       PROCEDURE DIVISION.
       Begin.
           MOVE ALPHA-NUM TO NUM-INT
           DISPLAY NUM-INT.
           MOVE ALPHA-NUM TO NUM-NON-INT
           DISPLAY NUM-NON-INT.
           MOVE ALPHA-NUM TO ALPHA
           DISPLAY ALPHA.